import {Injectable} from 'angular2/core';
import {Movie} from './movie'; 

@Injectable{}
export class MovieService{
	movies: Movie[] = [
		new Movie('Terminator', ['jeft', 'john', 'juju'], ['joel','ethan'], 1998),
		new Movie('Rambo', ['jeft2', 'john2', 'juju2'], ['joel2'], 1999),
		new Movie('Robocop', ['jeft3', 'john3', 'juju3'], ['joel3','ethan3'], 2000)
	];
	
	getAllMovies(){

		return this.movies; 
	}

	addMovie(movie){
		this.movies.push(movie);
	}
}