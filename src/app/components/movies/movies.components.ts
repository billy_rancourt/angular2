import {Component} from 'angular2/core';
import {MovieService} from './movies.services';
import {OnInit} from 'angular2/core';
import {Movie} from './movie';
import {FormBuilder} from 'angular2/common';
import {ControlGroup} from 'angular2/common';
import {Validators} from 'angular2/common';
import {Control} from 'angular2/common';

@Component({
	selector: "movies", //<movies></movies>
	templateUrl : 'app/components/movies/movies.components.html',
	providers: [MovieService]
})


export class Movies implements OnInit {

	movieService : MovieService;
	recentlyWatchedMovies: Movie[];
	nbOfMovies : Number;
	movie:Movie;
	myForm: ControlGroup;
	formBuilder: FormBuilder;

	constructor(movieService: MovieService, formBuilder:FormBuilder){
		this.movieService = movieService;
		this.formBuilder = formBuilder;
		
	}

	//recentlyWatchedMovies = ['Terminator', "Rambo", "Robocop"];
	
	addMovie(){
		/*
		this.movieService.addMovie(movie);
		this.nbOfMovies = this.recentlyWatchedMovies.length;
		*/

		console.log(this.myForm);

		let formValue = this.myForm.value;

		
		console.log(formValue);
		this.movie = new Movie();
		this.movie.title = formValue.title;
		this.movie.directors = formValue.directors;
		this.movie.actors = formValue.actors;
		this.movie.year = formValue.year;

		this.clearForm();

		this.movieService.addMovie(this.movie);
		this.nbOfMovies = this.recentlyWatchedMovies.length;

		this.movie = new Movie();
	}

	clearForm(){
		let controlsObj = this.myForm.controls;
		for (let prop in controlsObj){
			if(controlsObj.hasOwnProperty(prop)) {
				console.log(controlsObj[prop]);
				(controlsObj[prop] as Control).updateValue('');
			}
		}
	}

	ngOnInit(){
		this.recentlyWatchedMovies = this.movieService.getAllMovies();
		this.nbOfMovies = this.recentlyWatchedMovies.length;
		this.myForm = this.formBuilder.group({
			'title' : ['', Validators.required],
			'actors': [['']],
			'directors': [['']],
			'year': [2016, Validators.required]

		})
	}
	
}